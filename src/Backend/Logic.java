package Backend;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;


public class Logic {
	
	private Double multiply(Double a, Double b) {
		return a*b;
	}

	private Double divide(Double a, Double b) {
		return a/b;
	}
	
	private Double add(Double a, Double b) {
		return a+b;
	}
	
	private Double subtract(Double a, Double b) {
		return a-b;
	}
	
	private Queue<String> parseInput (String equation) {
		String [] input = equation.split("\\s+");
		Queue<String> instruction = new ConcurrentLinkedQueue<String>();
		for (String s: input)
			instruction.add(s);
		return instruction;
	}
	
	@SuppressWarnings("serial")
	private class OperatorException extends Exception {
		
		public OperatorException() {
			super("Not a valid operator");
		}
	}
	
	private Double calculate(Double a, Double b, String operator) throws OperatorException {
		Double result = null;
		switch (operator) {
			case "+":
				result = add(a,b);
				break;
			case "-":
				result = subtract(a,b);
				break;
			case "*":
				result = multiply(a,b);
				break;
			case "/":
				result = divide(a,b);
				break;
			default:
				throw new OperatorException();
		}
		
		return result;
	}
	
	public Double performCalculation(String equation) {
		Queue<String> instruction = parseInput(equation);
		//ASSUME: 1st operation is valid
		Double a, b, result = null;
		String operator;
		
		try {
			a = Double.parseDouble( instruction.remove() );
			operator = instruction.remove();
			b = Double.parseDouble( instruction.remove() );
			result = calculate(a,b,operator);
		} catch (NumberFormatException e) {
			System.err.println("Invalid number input.");
			System.exit(1);
		} catch (OperatorException e) {
			System.err.println(e.getMessage());
			System.exit(1);
		}
		
		while (instruction.peek() != null) {
			try {
				operator = instruction.remove();
				b = Double.parseDouble( instruction.remove() );
				result = calculate(result,b,operator);
			} catch (NumberFormatException e) {
				System.err.println("Invalid number input.");
				System.exit(1);
			} catch (OperatorException e) {
				System.err.println(e.getMessage());
				System.exit(1);
			}
		}
		return result;
	}
	
	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter an equation:");
		String input = reader.readLine();
		Double result = new Logic().performCalculation(input);
		System.out.println(input+" = "+result);
	}

}
